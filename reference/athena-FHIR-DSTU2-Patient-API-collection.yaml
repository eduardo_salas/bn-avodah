openapi: 3.0.0
info:
  description: Welcome to the More Disruption Please (MDP) API developer portal which gives third parties access to our API and sandbox to develop integrated solutions that will enhance the value and functionality offered by athenaNet®.
  title: athena - FHIR DSTU2 - Patient API collection
  version: '0.1'
components:
  securitySchemes:
    mdp_auth_preview:
      type: oauth2
      flows:
        clientCredentials:
          tokenUrl: 'https://api.preview.platform.athenahealth.com/oauth2/v1/token'
          scopes:
            athena/service/Athenanet.MDP.*: MDP Preview
    mdp_auth:
      type: oauth2
      flows:
        clientCredentials:
          tokenUrl: 'https://api.platform.athenahealth.com/oauth2/v1/token'
          scopes:
            athena/service/Athenanet.MDP.*: MDP Production
    bearer_auth:
      type: http
      scheme: bearer
servers:
  - url: 'https://api.preview.platform.athenahealth.com'
    description: Preview
  - url: 'https://api.platform.athenahealth.com'
    description: Production
tags:
  - name: FHIR DSTU2 - Patient
    description: ''
paths:
  '/v1/{practiceid}/{departmentid}/fhir/dstu2/Patient':
    get:
      summary: Find Patients
      description: List of Patient resources from the results of the search. Date filtering does not apply.
      parameters:
        - description: practiceid
          in: path
          name: practiceid
          required: true
          schema:
            type: integer
        - description: departmentid
          in: path
          name: departmentid
          required: true
          schema:
            type: integer
        - description: The given name associated with the patient.
          in: query
          name: given
          required: false
          schema:
            type: string
        - description: The system-code identifier of the patient.
          in: query
          name: identifier
          required: false
          schema:
            type: string
        - description: 'The patient ID.  Per the FHIR spec, this is "_id", not "patientid" as with other API calls.'
          in: query
          name: _id
          required: false
          schema:
            type: integer
        - description: The birthdate of the patient.
          in: query
          name: birthdate
          required: false
          schema:
            type: string
        - description: The name associated with the patient.
          in: query
          name: name
          required: false
          schema:
            type: string
        - description: The family name associated with the patient.
          in: query
          name: family
          required: false
          schema:
            type: string
        - description: The gender of the patient.
          in: query
          name: gender
          required: false
          schema:
            type: string
        - description: User name of the patient in the third party application.
          in: query
          name: THIRDPARTYUSERNAME
          required: false
          schema:
            type: string
        - description: When 'true' is passed we will collect relevant data and store in our database.
          in: query
          name: PATIENTFACINGCALL
          required: false
          schema:
            type: boolean
      responses:
        '200':
          content:
            application/json:
              schema:
                properties:
                  address:
                    description: Addresses for the individual
                    items:
                      properties:
                        city:
                          description: 'Name of city, town etc.'
                          type: string
                        country:
                          description: Country (can be ISO 3166 3 letter code)
                          type: string
                        district:
                          description: District name (aka county)
                          type: string
                        line:
                          description: 'Street name, number, direction & P.O. Box etc.'
                          type: string
                        period:
                          description: Time period when address was/is in use
                          type: string
                        postalcode:
                          description: Postal code for area
                          type: string
                        state:
                          description: Sub-unit of country (abbreviations ok)
                          type: string
                        text:
                          description: Text representation of the address
                          type: string
                        type:
                          description: postal | physical | both
                          type: string
                        use:
                          description: home | work | temp | old - purpose of this address
                          type: string
                      type: object
                    type: array
                  birthdate:
                    description: The date of birth for the individual
                    type: string
                  communication:
                    description: A list of Languages which may be used to communicate with the patient about his or her health
                    items:
                      properties:
                        language:
                          description: The language which can be used to communicate with the patient about his or her health
                          properties:
                            coding:
                              description: Language Coding
                              properties:
                                coding:
                                  description: Code defined by a terminology system
                                  items:
                                    properties:
                                      code:
                                        description: Symbol in syntax defined by the system
                                        type: string
                                      display:
                                        description: Representation defined by the system
                                        type: string
                                      system:
                                        description: Identity of the terminology system
                                        type: string
                                    type: object
                                  type: array
                                text:
                                  description: Plain text representation of the concept
                                  type: string
                              type: object
                            text:
                              description: Language Text
                              type: string
                          type: object
                        preferred:
                          description: Language preference indicator
                          enum:
                            - ''
                            - false
                            - true
                          type: string
                      type: object
                    type: array
                  extension:
                    description: Additional Content defined by implementations
                    items:
                      properties:
                        url:
                          description: Identifies the meaning of the extension
                          type: string
                        valuecodeableconcept:
                          description: Value of extension
                          properties:
                            coding:
                              description: Code defined by a terminology system
                              items:
                                properties:
                                  code:
                                    description: Symbol in syntax defined by the system
                                    type: string
                                  display:
                                    description: Representation defined by the system
                                    type: string
                                  system:
                                    description: Identity of the terminology system
                                    type: string
                                type: object
                              type: array
                            text:
                              description: Plain text representation of the concept
                              type: string
                          type: object
                      type: object
                    type: array
                  gender:
                    description: male | female | other | unknown
                    type: string
                  id:
                    description: Logical id of this artifact
                    type: string
                  identifier:
                    description: An identifier for this patient
                    properties:
                      system:
                        description: Identifier System
                        type: string
                      use:
                        description: Identifier Use
                        type: string
                      value:
                        description: Identifier Value
                        type: string
                    type: object
                  name:
                    description: A name associated with the patient
                    properties:
                      family:
                        description: List of family names for this patient
                        type: string
                      given:
                        description: List of given names for this patient
                        type: string
                      use:
                        description: Purpose of this name
                        type: string
                    type: object
                  resourcetype:
                    description: The type of resource
                    type: string
                  telecom:
                    description: A contact detail for the individual
                    properties:
                      system:
                        description: Telecom system
                        type: string
                      use:
                        description: Use of this telecom - home | work
                        type: string
                      value:
                        description: Telecom value
                        type: string
                    type: object
                type: object
          description: Success
      operationId: getPracticeidDepartmentidFhirDstu2Patient
      tags:
        - FHIR DSTU2 - Patient
  '/v1/{practiceid}/{departmentid}/fhir/dstu2/Patient/{patientid}':
    get:
      summary: Get a single patient
      description: Returns Patient Information. Date filtering does not apply.
      parameters:
        - description: practiceid
          in: path
          name: practiceid
          required: true
          schema:
            type: integer
        - description: departmentid
          in: path
          name: departmentid
          required: true
          schema:
            type: integer
        - description: patientid
          in: path
          name: patientid
          required: true
          schema:
            type: integer
        - description: User name of the patient in the third party application.
          in: query
          name: THIRDPARTYUSERNAME
          required: false
          schema:
            type: string
        - description: When 'true' is passed we will collect relevant data and store in our database.
          in: query
          name: PATIENTFACINGCALL
          required: false
          schema:
            type: boolean
      responses:
        '200':
          content:
            application/json:
              schema:
                properties:
                  address:
                    description: Addresses for the individual
                    items:
                      properties:
                        city:
                          description: 'Name of city, town etc.'
                          type: string
                        country:
                          description: Country (can be ISO 3166 3 letter code)
                          type: string
                        district:
                          description: District name (aka county)
                          type: string
                        line:
                          description: 'Street name, number, direction & P.O. Box etc.'
                          type: string
                        period:
                          description: Time period when address was/is in use
                          type: string
                        postalcode:
                          description: Postal code for area
                          type: string
                        state:
                          description: Sub-unit of country (abbreviations ok)
                          type: string
                        text:
                          description: Text representation of the address
                          type: string
                        type:
                          description: postal | physical | both
                          type: string
                        use:
                          description: home | work | temp | old - purpose of this address
                          type: string
                      type: object
                    type: array
                  birthdate:
                    description: The date of birth for the individual
                    type: string
                  communication:
                    description: A list of Languages which may be used to communicate with the patient about his or her health
                    items:
                      properties:
                        language:
                          description: The language which can be used to communicate with the patient about his or her health
                          properties:
                            coding:
                              description: Language Coding
                              properties:
                                coding:
                                  description: Code defined by a terminology system
                                  items:
                                    properties:
                                      code:
                                        description: Symbol in syntax defined by the system
                                        type: string
                                      display:
                                        description: Representation defined by the system
                                        type: string
                                      system:
                                        description: Identity of the terminology system
                                        type: string
                                    type: object
                                  type: array
                                text:
                                  description: Plain text representation of the concept
                                  type: string
                              type: object
                            text:
                              description: Language Text
                              type: string
                          type: object
                        preferred:
                          description: Language preference indicator
                          enum:
                            - ''
                            - false
                            - true
                          type: string
                      type: object
                    type: array
                  extension:
                    description: Additional Content defined by implementations
                    items:
                      properties:
                        url:
                          description: Identifies the meaning of the extension
                          type: string
                        valuecodeableconcept:
                          description: Value of extension
                          properties:
                            coding:
                              description: Code defined by a terminology system
                              items:
                                properties:
                                  code:
                                    description: Symbol in syntax defined by the system
                                    type: string
                                  display:
                                    description: Representation defined by the system
                                    type: string
                                  system:
                                    description: Identity of the terminology system
                                    type: string
                                type: object
                              type: array
                            text:
                              description: Plain text representation of the concept
                              type: string
                          type: object
                      type: object
                    type: array
                  gender:
                    description: male | female | other | unknown
                    type: string
                  id:
                    description: Logical id of this artifact
                    type: string
                  identifier:
                    description: An identifier for this patient
                    properties:
                      system:
                        description: Identifier System
                        type: string
                      use:
                        description: Identifier Use
                        type: string
                      value:
                        description: Identifier Value
                        type: string
                    type: object
                  name:
                    description: A name associated with the patient
                    properties:
                      family:
                        description: List of family names for this patient
                        type: string
                      given:
                        description: List of given names for this patient
                        type: string
                      use:
                        description: Purpose of this name
                        type: string
                    type: object
                  resourcetype:
                    description: The type of resource
                    type: string
                  telecom:
                    description: A contact detail for the individual
                    properties:
                      system:
                        description: Telecom system
                        type: string
                      use:
                        description: Use of this telecom - home | work
                        type: string
                      value:
                        description: Telecom value
                        type: string
                    type: object
                type: object
          description: Success
      operationId: getPracticeidDepartmentidFhirDstu2PatientPatientid
      tags:
        - FHIR DSTU2 - Patient
  '/v1/{practiceid}/{brandid}/{chartsharinggroupid}/fhir/dstu2/Patient':
    get:
      summary: Find patients by brand and chart
      description: List of Patient resources from the results of the search. Date filtering does not apply.
      parameters:
        - description: practiceid
          in: path
          name: practiceid
          required: true
          schema:
            type: integer
        - description: brandid
          in: path
          name: brandid
          required: true
          schema:
            type: integer
        - description: chartsharinggroupid
          in: path
          name: chartsharinggroupid
          required: true
          schema:
            type: integer
        - description: 'The patient ID.  Per the FHIR spec, this is "_id", not "patientid" as with other API calls.'
          in: query
          name: _id
          required: true
          schema:
            type: integer
        - description: User name of the patient in the third party application.
          in: query
          name: THIRDPARTYUSERNAME
          required: false
          schema:
            type: string
        - description: When 'true' is passed we will collect relevant data and store in our database.
          in: query
          name: PATIENTFACINGCALL
          required: false
          schema:
            type: boolean
      responses:
        '200':
          content:
            application/json:
              schema:
                properties:
                  address:
                    description: Addresses for the individual
                    items:
                      properties:
                        city:
                          description: 'Name of city, town etc.'
                          type: string
                        country:
                          description: Country (can be ISO 3166 3 letter code)
                          type: string
                        district:
                          description: District name (aka county)
                          type: string
                        line:
                          description: 'Street name, number, direction & P.O. Box etc.'
                          type: string
                        period:
                          description: Time period when address was/is in use
                          type: string
                        postalcode:
                          description: Postal code for area
                          type: string
                        state:
                          description: Sub-unit of country (abbreviations ok)
                          type: string
                        text:
                          description: Text representation of the address
                          type: string
                        type:
                          description: postal | physical | both
                          type: string
                        use:
                          description: home | work | temp | old - purpose of this address
                          type: string
                      type: object
                    type: array
                  birthdate:
                    description: The date of birth for the individual
                    type: string
                  communication:
                    description: A list of Languages which may be used to communicate with the patient about his or her health
                    items:
                      properties:
                        language:
                          description: The language which can be used to communicate with the patient about his or her health
                          properties:
                            coding:
                              description: Language Coding
                              properties:
                                coding:
                                  description: Code defined by a terminology system
                                  items:
                                    properties:
                                      code:
                                        description: Symbol in syntax defined by the system
                                        type: string
                                      display:
                                        description: Representation defined by the system
                                        type: string
                                      system:
                                        description: Identity of the terminology system
                                        type: string
                                    type: object
                                  type: array
                                text:
                                  description: Plain text representation of the concept
                                  type: string
                              type: object
                            text:
                              description: Language Text
                              type: string
                          type: object
                        preferred:
                          description: Language preference indicator
                          enum:
                            - ''
                            - false
                            - true
                          type: string
                      type: object
                    type: array
                  extension:
                    description: Additional Content defined by implementations
                    items:
                      properties:
                        url:
                          description: Identifies the meaning of the extension
                          type: string
                        valuecodeableconcept:
                          description: Value of extension
                          properties:
                            coding:
                              description: Code defined by a terminology system
                              items:
                                properties:
                                  code:
                                    description: Symbol in syntax defined by the system
                                    type: string
                                  display:
                                    description: Representation defined by the system
                                    type: string
                                  system:
                                    description: Identity of the terminology system
                                    type: string
                                type: object
                              type: array
                            text:
                              description: Plain text representation of the concept
                              type: string
                          type: object
                      type: object
                    type: array
                  gender:
                    description: male | female | other | unknown
                    type: string
                  id:
                    description: Logical id of this artifact
                    type: string
                  identifier:
                    description: An identifier for this patient
                    properties:
                      system:
                        description: Identifier System
                        type: string
                      use:
                        description: Identifier Use
                        type: string
                      value:
                        description: Identifier Value
                        type: string
                    type: object
                  name:
                    description: A name associated with the patient
                    properties:
                      family:
                        description: List of family names for this patient
                        type: string
                      given:
                        description: List of given names for this patient
                        type: string
                      use:
                        description: Purpose of this name
                        type: string
                    type: object
                  resourcetype:
                    description: The type of resource
                    type: string
                  telecom:
                    description: A contact detail for the individual
                    properties:
                      system:
                        description: Telecom system
                        type: string
                      use:
                        description: Use of this telecom - home | work
                        type: string
                      value:
                        description: Telecom value
                        type: string
                    type: object
                type: object
          description: Success
      operationId: getPracticeidBrandidChartsharinggroupidFhirDstu2Patient
      tags:
        - FHIR DSTU2 - Patient
  '/v1/{practiceid}/{brandid}/{chartsharinggroupid}/fhir/dstu2/Patient/{patientid}':
    get:
      summary: Get a single patient
      description: Returns Patient Information. Date filtering does not apply.
      parameters:
        - description: practiceid
          in: path
          name: practiceid
          required: true
          schema:
            type: integer
        - description: brandid
          in: path
          name: brandid
          required: true
          schema:
            type: integer
        - description: patientid
          in: path
          name: patientid
          required: true
          schema:
            type: integer
        - description: chartsharinggroupid
          in: path
          name: chartsharinggroupid
          required: true
          schema:
            type: integer
      responses:
        '200':
          content:
            application/json:
              schema:
                properties:
                  address:
                    description: Addresses for the individual
                    items:
                      properties:
                        city:
                          description: 'Name of city, town etc.'
                          type: string
                        country:
                          description: Country (can be ISO 3166 3 letter code)
                          type: string
                        district:
                          description: District name (aka county)
                          type: string
                        line:
                          description: 'Street name, number, direction & P.O. Box etc.'
                          type: string
                        period:
                          description: Time period when address was/is in use
                          type: string
                        postalcode:
                          description: Postal code for area
                          type: string
                        state:
                          description: Sub-unit of country (abbreviations ok)
                          type: string
                        text:
                          description: Text representation of the address
                          type: string
                        type:
                          description: postal | physical | both
                          type: string
                        use:
                          description: home | work | temp | old - purpose of this address
                          type: string
                      type: object
                    type: array
                  birthdate:
                    description: The date of birth for the individual
                    type: string
                  communication:
                    description: A list of Languages which may be used to communicate with the patient about his or her health
                    items:
                      properties:
                        language:
                          description: The language which can be used to communicate with the patient about his or her health
                          properties:
                            coding:
                              description: Language Coding
                              properties:
                                coding:
                                  description: Code defined by a terminology system
                                  items:
                                    properties:
                                      code:
                                        description: Symbol in syntax defined by the system
                                        type: string
                                      display:
                                        description: Representation defined by the system
                                        type: string
                                      system:
                                        description: Identity of the terminology system
                                        type: string
                                    type: object
                                  type: array
                                text:
                                  description: Plain text representation of the concept
                                  type: string
                              type: object
                            text:
                              description: Language Text
                              type: string
                          type: object
                        preferred:
                          description: Language preference indicator
                          enum:
                            - ''
                            - false
                            - true
                          type: string
                      type: object
                    type: array
                  extension:
                    description: Additional Content defined by implementations
                    items:
                      properties:
                        url:
                          description: Identifies the meaning of the extension
                          type: string
                        valuecodeableconcept:
                          description: Value of extension
                          properties:
                            coding:
                              description: Code defined by a terminology system
                              items:
                                properties:
                                  code:
                                    description: Symbol in syntax defined by the system
                                    type: string
                                  display:
                                    description: Representation defined by the system
                                    type: string
                                  system:
                                    description: Identity of the terminology system
                                    type: string
                                type: object
                              type: array
                            text:
                              description: Plain text representation of the concept
                              type: string
                          type: object
                      type: object
                    type: array
                  gender:
                    description: male | female | other | unknown
                    type: string
                  id:
                    description: Logical id of this artifact
                    type: string
                  identifier:
                    description: An identifier for this patient
                    properties:
                      system:
                        description: Identifier System
                        type: string
                      use:
                        description: Identifier Use
                        type: string
                      value:
                        description: Identifier Value
                        type: string
                    type: object
                  name:
                    description: A name associated with the patient
                    properties:
                      family:
                        description: List of family names for this patient
                        type: string
                      given:
                        description: List of given names for this patient
                        type: string
                      use:
                        description: Purpose of this name
                        type: string
                    type: object
                  resourcetype:
                    description: The type of resource
                    type: string
                  telecom:
                    description: A contact detail for the individual
                    properties:
                      system:
                        description: Telecom system
                        type: string
                      use:
                        description: Use of this telecom - home | work
                        type: string
                      value:
                        description: Telecom value
                        type: string
                    type: object
                type: object
          description: Success
      operationId: getPracticeidBrandidChartsharinggroupidFhirDstu2PatientPatientid
      tags:
        - FHIR DSTU2 - Patient
security:
  - mdp_auth_preview:
      - athena/service/Athenanet.MDP.*
    mdp_auth:
      - athena/service/Athenanet.MDP.*
    bearer_auth: []
